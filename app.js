const express = require('express')
require('dotenv').config({ path: __dirname + '/.env' })
const cors = require('cors');
const userRouter = require('./routes/user')
const taskRouter = require('./routes/task')

const app = express()
app.use(cors());
require('./db/mongoose')

app.use(express.json())
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials");
    res.header("Access-Control-Allow-Credentials", "true");
    next();
});
app.use(userRouter)
app.use(taskRouter)

module.exports = app